const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/*
Check if the email is already exists
1. Use the find() method to find duplicate emails
2. error handling, if no duplicate found, return false, else return true



Important Note: best practice to return a result is to use a boolean or return an object or array of object. because string is limited in our backend, and can't be connected to our frontend
*/


module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0){
			return true;

		}else{
			//no duplicate email found
			return false;
		}
	})
}

//User Registration
/*
Steps:
1. Create a new User Object using the mongoose model and the info from the request body.
2. Make sure that the password is encrypted
3. Save the new user to the databse
*/

module.exports.registerUser = (reqBody) =>{
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: /*reqBody.password,*/ bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	//Save
	return newUser.save().then((user, error) => {
		if (error){
			return false;
		}else{
			//user registration is successful
			return true
		}
	})
}


//User Authentication
/*
Steps:
1. Check if the user email exist in our database. If the user doesn't exist, return false
2. if the user exists, compare the password provided in the login form with the password stored in the database
3. Generate/return a jsonwebtoken if the user is successfully logged in and return false if not
*/


module.exports.loginUser = (reqBody) => {
	//findOne() method returns the first record in the collection that matches the search criteria
	//we use findOne() instead of "find" method which returns all records that match the search criteria
	return User.findOne({ email: reqBody.email }).then(result => {
		if (result == null){
			return false;
		}else{
			//User exists
			//The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			//pag may is means boolean ang inaasahang value
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

		//If the password match/result of the above code is true
		if(isPasswordCorrect){
			//generate access token//pikachu
			//the mongoose toObject()method converts the mongoose object into a plain javascript object 
			return { accessToken: auth.createAccessToken(result.toObject())}
		}else{
			//password do not match
			return false
		}


		}
	})
}



//activity starts here


/*module.exports.getProfile = (taskId) => {
	return User.findById(taskId).then((getProfile, err) =>{
	if(err){
	console.log(err);
	return false;
}else{
	return getProfile;
}

})
}
*/


	/*module.exports.getProfile = (reqBody) => {
	
	return User.findOne({ _id: reqBody._id }).then(result => {
		let results = {
		id: result.id,
    	firstName: result.firstName,
    	lastName: result.lastName,
   		age: result.age,
    	gender: result.gender,
    	email: result.email,
    	
    	mobileNo: result.mobileNo,
    	isAdmin: result.isAdmin,
    	enrollments: result.enrollments,
		}
return result;
	})
}*/

/*

module.exports.getProfile = (reqBody) => {
	return User.findOne({ id: reqBody._id }).then(result => {
		
		return result
	})
}*/


/*module.exports.getProfile = (reqBody) => {
	return User.findOne({ id: reqBody._id }).then(result => {
		let results = {
		_id: result.id,
    	firstName: result.firstName,
    	lastName: result.lastName,
   		age: result.age,
    	gender: result.gender,
    	email: result.email,
    	mobileNo: result.mobileNo,
    	isAdmin: result.isAdmin,
    	enrollments: result.enrollments,
		}
return results;
	
	})
}*/


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		
		result.password = "";

		
		return result;

	});

};



 



//Activity Solution

/*module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	})
}*/

//User Authentication using JSON Web Token




//Thursday activity











