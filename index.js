const express = require("express")
const mongoose = require("mongoose");
//Allows us to control app's Cross Origin Resource Sharing settings
const cors = require("cors");
const courseRoutes = require("./routes/course");


//server set up
const app = express();
const port = 4000;
//Allows all resources/origin to access our backend application
//Enable all course
//enable all is not recommended
app.use(cors());
const userRoutes = require("./routes/user")




/*let corsOptions = {
	origin: "https://dscscasd.com"
	origin: ["http://localhost:3000", "http://localhost:8000"]
}
*/

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Defines the "/api/users" string to be included for all routes defined in the "user" route file
app.use("/api/users", userRoutes)
//another routes for the course later

//after holiday added. thursday
app.use("/api/courses", courseRoutes)

//Database connection
mongoose.connect("mongodb+srv://admin:admin@cluster0.ek4rh.mongodb.net/batch144_booking_system?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("Now connected to MongoDB Atlas"))



app.listen(port, () => console.log(`API server is now listening to port ${port}`))