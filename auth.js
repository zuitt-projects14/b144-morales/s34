//allow us to get access to the token
//to secure na authenticated user ang nag lolog in

//JSON web token are standard for sending info between our app in a secure manner.
//will allow us to gain access to methods that will help us to create a JSON web token
//we are using secret code that only server can understand

const jwt = require("jsonwebtoken");
const secret = "CrushAkoNgCrushKo";

//JWT is a way of securely passing info from the server to the frontend or to the other parts of the server
//info is kept secure through the use of the secret code
//only the system that knows the secret code that can decode the encrypted info.

//Token Creation
//Analogy: Pack the gift and provide a lock with the secret code as the key


module.exports.createAccessToken = (user) => {
	//Data will be received from the registration form
	//when the users log in, a token will be created with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	//generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {})
}

//User authentication with JSON Web Token

//Handling verification
//Token Verification

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		console.log(token);

		//"Bearer" 
		token = token.slice(7, token.length);
		//Validate the token using "verify" method
		return jwt.verify(token, secret, (err, data) => {
			//if our JWT is not valid
			if(err){
				return res.send({auth: "failed"});
			}else{

				next();
			}
		})
	}else{
		//if token does not exist
		return res.send({auth: "failed"});
	}
}

// Token decryption

module.exports.decode = (token) => {
	//Token receive and is not undefined
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null;
			}else{
		
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}else{
		return null;
	}
}



























